from weakref import WeakValueDictionary
cache_memory_leak = WeakValueDictionary()


_cache = WeakValueDictionary() #Global dict for storage

def memoize(key):
    def _decorating_wrapper(func):
        def _caching_wrapper(*args, **kwargs):
            #cache_key = normalize_key(key, args, kwargs)

            #if _cache.has_key(cache_key):
            #   return _cache[cache_key]

            #ret = func(*args, **kwargs)
            #_cache[cache_key] = ret
            #return ret
            if not key in _cache:
                value = func(*args, **kwargs)
                _cache[key] = value
            return _cache[key]
        return _caching_wrapper
    return _decorating_wrapper

@memoize('funccccc')
def myfunc():
    x=200
    y=100

    print x ** 2 - y ** 2
"""
myfunc()
print _cache
print dict(_cache)
del _cache
print dict(_cache)
"""

#######################################################33
from pympler import muppy, summary
from collections import defaultdict
from gc import get_objects
all_objects = muppy.get_objects()
from pympler import tracker
tr = tracker.SummaryTracker()
tr.print_diff()
def fun():
    before = defaultdict(int)
    after = defaultdict(int)
    for i in get_objects():
        before[type(i)] += 1
    leaked_things = [[x] for x in range(10)]
    for i in get_objects():
        after[type(i)] += 1


    print [(k, after[k] - before[k]) for k in after if after[k] - before[k]]
    """
    for k in after:
        print after[k], ":::::::::", before[k]
        print after[k]-before[k]
        if after[k]- before[k]:
            print k, after[k]-before[k]
    """
tr.print_diff()
fun()
tr.print_diff()
