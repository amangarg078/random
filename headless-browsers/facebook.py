import string
import random
import zipfile
from time import sleep

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import ElementNotVisibleException, NoSuchElementException, WebDriverException



def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

random_string = id_generator()

#create a chrome extension to handle proxy authentication
manifest_json = """
{
    "version": "1.0.0",
    "manifest_version": 2,
    "name": "Chrome Proxy",
    "permissions": [
        "proxy",
        "tabs",
        "unlimitedStorage",
        "storage",
        "<all_urls>",
        "webRequest",
        "webRequestBlocking"
    ],
    "background": {
        "scripts": ["background.js"]
    },
    "minimum_chrome_version":"22.0.0"
}
"""

background_js = """
var config = {
        mode: "fixed_servers",
        rules: {
          singleProxy: {
            scheme: "http",
            host: "%(host)s",
            port: parseInt(%(port)s)
          },
          bypassList: ["foobar.com"]
        }
      };

chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

function callbackFn(details) {
    return {
        authCredentials: {
            username: "%(username)s",
            password: "%(password)s"
        }
    };
}

chrome.webRequest.onAuthRequired.addListener(
            callbackFn,
            {urls: ["<all_urls>"]},
            ['blocking']
);
""" % {'host':'139.59.78.112', 'port':'3024', 'username':'aman', 'password':'GXzrmQrw'}

#generate an extension for the user
pluginfile = 'proxy_auth_plugin_{}.zip'.format('aman')

with zipfile.ZipFile(pluginfile, 'w') as zp:
    zp.writestr("manifest.json", manifest_json)
    zp.writestr("background.js", background_js)


chrome_options = Options()
prefs = {"profile.default_content_setting_values.notifications" : 2,
'credentials_enable_service': False,
    'profile': {
        'password_manager_enabled': False
    }
}
chrome_options.add_argument("--start-maximized")
chrome_options.add_experimental_option("prefs",prefs)
chrome_options.add_extension("proxy_auth_plugin_{}.zip".format('aman'))
#driver = webdriver.Chrome(chrome_options=chrome_options)
driver = webdriver.Remote("http://172.17.42.1:5555/wd/hub", chrome_options.to_capabilities())


driver.get('https://www.google.com/search?q=what+is+my+ip')
ip = driver.find_elements_by_xpath("//*[contains(text(), '{}')]".format('139.59.78.112'))
print ip
assert ip != None
driver.get('https://www.facebook.com')
driver.implicitly_wait(2)

#admin login form
driver.find_element_by_xpath("//input[@id='email']").send_keys('mediplat12@gmail.com')
driver.find_element_by_xpath("//input[@id='pass']").send_keys('123qweasdzxc!')
driver.implicitly_wait(2)
driver.find_element_by_xpath("//input[@value='Log In']").click()
driver.implicitly_wait(5)
#driver.get_screenshot_as_file('facebook_{}.png'.format(random_string))
#driver.implicitly_wait(2)
#driver.get('https://www.facebook.com')
#driver.implicitly_wait(2)
j=0
"""
for i in range(0,5):
    driver.execute_script("window.scrollTo({0}, {1});".format(j,j+500))
    j=j+200
    time.sleep(1)
"""
post_liked=[]
driver.get('https://www.facebook.com')
sleep(5)
for i in range(15):
    driver.find_element_by_tag_name('body').send_keys(Keys.DOWN)

sleep(5)
try:
    more_stories_link = driver.find_element_by_xpath("//span[@class='fsxl fcg']/ancestor::a/")
    print 'more_stories_link',more_stories_link
    more_stories_link.click()
except NoSuchElementException:
    print "oh no"
    pass
last_liked=driver.find_element_by_tag_name('body')
likes_placeholder = []
random.seed(1000)
for p in range(2):

    likes_on_screen = driver.find_elements_by_xpath("//a[contains(@class,'UFILikeLink')]")
    pages = driver.find_elements_by_xpath("//a[contains(@data-hovercard,'/ajax/hovercard/page.php')]")

    #print posts, len(posts)
    likes = [x for x in likes_on_screen if x not in likes_placeholder]
    print likes, len(likes)
    random_likes = random.randint(len(likes)/2,len(likes)-1)
    print "random_number", random_likes
    range_random_likes = random.sample(range(0, random_likes), (3*random_likes)/4)
    range_random_likes.sort()
    print "range_random_likes",range_random_likes

    for i in range_random_likes:

        try:
            print "likes[i]: ",likes[i]
            likes[i].send_keys(Keys.NULL)
            likes[i].click()
            sleep(1)
            post_liked.append(likes[i])
            print "Clicked!!"
            last_liked=likes[i]
            for i in range(25):
                last_liked.send_keys(Keys.DOWN)
            sleep(1)
            print "last_liked ",last_liked
        except (WebDriverException, ElementNotVisibleException):
            print "element not visible"
            for i in range(30):
                try:
                    last_liked.send_keys(Keys.DOWN)
                except ElementNotVisibleException:
                    driver.find_element_by_tag_name('body').send_keys(Keys.DOWN)
                    pass
            sleep(1)
            pass

    likes_placeholder = likes_on_screen
    sleep(2)

    """
    for p in range(2):
        #driver.find_element_by_tag_name('body').send_keys(Keys.DOWN)
        print "window.scrollTo({0}, {1});".format(j,j+200)
        driver.execute_script("window.scrollTo({0}, {1});".format(j,j+200))
        j=j+200

        sleep(1)
    """
"""
random_posts = random.randint(1,len(posts))
for i in range(random_posts):
    random_post = random.randint(1, random_posts)
    print random_post
    print posts[random_post]
    try:
        posts[random_post].send_keys(Keys.CONTROL + Keys.RETURN)
        for i in range(0,5):
    driver.execute_script("window.scrollTo({0}, {1});".format(j,j+500))
    j=j+200
    sleep(1)



#close the browser
driver.close()
"""