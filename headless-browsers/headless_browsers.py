from selenium import webdriver
import string
import random

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

random_string = id_generator()

options = webdriver.ChromeOptions()

#specify to use google-chrome-unstable and use headless version
options.binary_location = '/usr/bin/google-chrome-unstable'
options.add_argument('headless')

# initialize the driver
driver = webdriver.Chrome(chrome_options=options)

# set the window size (optional)
options.add_argument('window-size=1200x600')

driver.get('http://127.0.0.1:8000/admin/')
driver.implicitly_wait(2)

#admin login form
driver.find_element_by_xpath("//input[@id='id_username']").send_keys('admin')
driver.find_element_by_xpath("//input[@id='id_password']").send_keys('123qweasd')
driver.find_element_by_xpath('//input[@type="submit"]').click()
driver.get_screenshot_as_file('main-page_{}.png'.format(random_string))
driver.implicitly_wait(2)

#click on Add User button and add a user
driver.find_element_by_xpath("//tr[contains(@class,'user')]/td/a").click()
driver.find_element_by_xpath("//input[@id='id_username']").send_keys('user_{}'.format(random_string))
driver.find_element_by_xpath("//input[@id='id_password1']").send_keys('123qweasd')
driver.find_element_by_xpath("//input[@id='id_password2']").send_keys('123qweasd')
driver.find_element_by_xpath("//input[@name='_save']").click()
driver.get_screenshot_as_file('user_{}.png'.format(random_string))

#scroll functionality
driver.get('http://127.0.0.1:8000/admin/')
driver.implicitly_wait(2)
driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
driver.get_screenshot_as_file('scroll_{}'.format(random_string))

#close the browser
driver.close()