from selenium import webdriver
import string
import random

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

random_string = id_generator()

options = webdriver.ChromeOptions()

#specify to use google-chrome-unstable and use headless version
options.binary_location = '/usr/bin/google-chrome-unstable'
options.add_argument('headless')

# initialize the driver
driver = webdriver.Chrome(chrome_options=options)

# set the window size (optional)
options.add_argument('window-size=1200x600')

driver.get('https://www.twitter.com')
driver.implicitly_wait(2)

#admin login form
driver.find_element_by_xpath("//a[@class='Button StreamsLogin js-login']").click()
driver.find_element_by_xpath("//input[@name='session[username_or_email]']").send_keys('aman_test_PM')
driver.find_element_by_xpath("//input[@name='session[password]']").send_keys('123qweasd')
driver.find_element_by_xpath('//input[@value="Log in"]').click()
driver.get_screenshot_as_file('main-page_{}.png'.format(random_string))
driver.implicitly_wait(2)

#click on Add User button and add a user
driver.find_element_by_id("tweet-box-home-timeline").send_keys("Link in park with Linkin Park watching Lincoln park a Lincoln")
driver.find_element_by_xpath("//div[@class='TweetBoxToolbar']/div/button[contains(@class,'tweet-btn')]").click()
driver.get_screenshot_as_file('tweet_{}.png'.format(random_string))

driver.find_element_by_id('search-query').send_keys('@pakalupapito')
driver.find_element_by_xpath("//a[contains(@href,'/search?f=')]").click()


#close the browser
driver.close()