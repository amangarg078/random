from fabric.api import env, cd, run, prefix, sudo
from fabric.contrib.files import sed

env.hosts = ['162.243.247.83']
env.user = 'root'
def update_django_project():
    """ Updates the remote django project.
    """
    with cd('/home/feedfetcher/aman/monitoring'):
        run('git pull')
        with prefix('source /home/feedfetcher/aman/monitoring_env/bin/activate'):
            run('pip install -r requirements.txt')
            run('python manage.py makemigrations')
            run('python manage.py migrate') # if you use south
            run('python manage.py collectstatic --noinput')

def restart_webserver():
    """ Restarts remote nginx and supervisor.
    """
    run("supervisorctl restart aman_monitoring ")
    run("service nginx restart")


def deploy():
    """ Deploy Django Project.
    """
    update_django_project()
    restart_webserver()

def edit_file():
    settings_path = '/home/feedfetcher/aman/monitoring/monitoring/settings.py'
    sed(settings_path, "DEBUG = True", "DEBUG = False")