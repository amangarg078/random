#!/bin/bash

NAME="aman_monitoring"                                  # Name of the application
DJANGODIR=/home/feedfetcher/aman/monitoring/             # Django project directory
SOCKFILE=/home/feedfetcher/aman/monitoring/run/gunicorn.sock  # we will communicte using this unix socket
USER=feedfetcher                                        # the user to run as                                    # the group to run as
NUM_WORKERS=3                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=monitoring.local_settings             # which settings file should Django use
DJANGO_WSGI_MODULE=monitoring.wsgi                     # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /home/feedfetcher/aman/monitoring_env/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /home/feedfetcher/aman/monitoring_env/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER
  --bind=0.0.0.0:8088 \
  --log-level=debug \
